(function () {
    'use strict';

    function Home() {}

    Home.prototype = {

        preload: function () {

        },

        create: function () {
            this.game.state.start('preloader');
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Home = Home;

}());