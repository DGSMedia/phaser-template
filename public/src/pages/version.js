(function () {
    'use strict';

    function Version() {}

    Version.prototype = {

        preload: function () {

        },

        create: function () {
            this.game.state.start('preloader');
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Version = Version;

}());